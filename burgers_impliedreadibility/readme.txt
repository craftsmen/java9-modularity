# create the burger model
javac --module-path mods -d mods/nl.craftsmen.burgers.model src/nl.craftsmen.burgers.model/nl/craftsmen/burgers/model/Burger.java src/nl.craftsmen.burgers.model/module-info.java

# create the macDonalds module
javac --module-path mods -d mods/nl.craftsmen.burgers.macdonalds src/nl.craftsmen.burgers.macdonalds/nl/craftsmen/burgers/macdonalds/internal/Kitchen.java src/nl.craftsmen.burgers.macdonalds/nl/craftsmen/burgers/macdonalds/MacDonalds.java src/nl.craftsmen.burgers.macdonalds/module-info.java

# create the consumer main application
javac --module-path mods -d mods/nl.craftsmen.burgers.consumer src/nl.craftsmen.burgers.consumer/nl/craftsmen/burgers/consumer/Main.java src/nl.craftsmen.burgers.consumer/module-info.java

# and run the consumer main application
java --module-path mods -m nl.craftsmen.burgers.consumer/nl.craftsmen.burgers.consumer.Main

result: the BigMac is served to the consumer!!!