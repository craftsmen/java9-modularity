package nl.craftsmen.burgers.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Burger{ 
	private String name;
	private List<String> ingredients=new ArrayList<>();	
	
	public Burger(String name){
		this.name=name;
	}
	
	public void addIngredients(String... ingredients){
		this.ingredients.addAll(Arrays.asList(ingredients));
	}

	public String getName() {
		return name;
	}

	public List<String> getIngredients() {
		return ingredients;
	};
}