package nl.craftsmen.burgers.macdonalds;

import nl.craftsmen.burgers.macdonalds.internal.Kitchen;
import nl.craftsmen.burgers.model.Burger;

public class MacDonalds {
	private Kitchen kitchen = new Kitchen();
	
	public Burger serveBurger(){
		return kitchen.createBigMac();
	}
}