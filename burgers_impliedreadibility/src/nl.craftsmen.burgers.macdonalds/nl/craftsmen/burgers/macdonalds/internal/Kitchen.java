package nl.craftsmen.burgers.macdonalds.internal;

import nl.craftsmen.burgers.model.Burger;

public class Kitchen{
	
	public Burger createBigMac(){
		Burger burger = new Burger("Big Mac");
		burger.addIngredients("Bun", "Beef patty", "Lettuce", "Cheese", "Pickles", "Unions", "Sauce");
		return burger;
	}
}