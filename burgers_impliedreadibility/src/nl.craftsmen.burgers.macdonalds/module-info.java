 module nl.craftsmen.burgers.macdonalds { 
	 requires public nl.craftsmen.burgers.model; // the module nl.craftsmen.burgers.model is required and it is made readible to the module nl.craftsmen.burgers.consumer because it requires (reads) the module nl.craftsmen.burgers.macdonalds and there fore readibility of nl.craftsmen.burgers.model is implied
	 exports nl.craftsmen.burgers.macdonalds;
 }