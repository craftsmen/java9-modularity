package nl.craftsmen.burgers.consumer;

import nl.craftsmen.burgers.macdonalds.MacDonalds;

public class Main {

	public static void main(String[] args) {
		MacDonalds md = new MacDonalds();
				
		System.out.println("Macdonalds gave me a " + md.serveBurger().getName());
	}
}