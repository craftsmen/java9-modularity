package nl.craftsmen.burgers.consumer;

import java.util.ServiceLoader;
import nl.craftsmen.burgers.restaurant.api.Restaurant;

public class Main {

	public static void main(String[] args) {
		ServiceLoader<Restaurant> serviceloader = ServiceLoader.load(Restaurant.class);

		System.out.println("Available burgers to the consumer:");
		for (Restaurant restaurant : serviceloader) {
			System.out.println("- " + restaurant.serveBurger());
		}
	}
}