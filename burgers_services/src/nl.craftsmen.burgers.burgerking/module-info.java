 module nl.craftsmen.burgers.burgerking { 
	 requires nl.craftsmen.burgers.restaurant;
	 provides nl.craftsmen.burgers.restaurant.api.Restaurant 
	 	with nl.craftsmen.burgers.burgerking.BurgerKing; 
 }