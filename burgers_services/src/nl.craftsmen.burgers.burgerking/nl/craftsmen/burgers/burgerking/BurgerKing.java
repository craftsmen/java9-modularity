package nl.craftsmen.burgers.burgerking;

import nl.craftsmen.burgers.burgerking.internal.Kitchen;
import nl.craftsmen.burgers.restaurant.api.Restaurant;

public class BurgerKing implements Restaurant{
	private Kitchen kitchen = new Kitchen();
	
	public String serveBurger(){
		return kitchen.createBurger();
	}
}