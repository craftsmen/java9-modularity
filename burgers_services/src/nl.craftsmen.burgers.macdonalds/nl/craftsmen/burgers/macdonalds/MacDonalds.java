package nl.craftsmen.burgers.macdonalds;

import nl.craftsmen.burgers.restaurant.api.Restaurant;
import nl.craftsmen.burgers.macdonalds.internal.Kitchen;

public class MacDonalds implements Restaurant{
	private Kitchen kitchen = new Kitchen();
	
	public String serveBurger(){
		return kitchen.createBurger();
	}
}