 module nl.craftsmen.burgers.macdonalds { 
	 requires nl.craftsmen.burgers.restaurant;
	 provides nl.craftsmen.burgers.restaurant.api.Restaurant 
	 	with nl.craftsmen.burgers.macdonalds.MacDonalds; 
 }