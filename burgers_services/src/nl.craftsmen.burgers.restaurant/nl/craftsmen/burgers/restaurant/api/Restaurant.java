package nl.craftsmen.burgers.restaurant.api;

public interface Restaurant{
	
	public String serveBurger();
}