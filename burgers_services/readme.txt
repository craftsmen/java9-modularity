# create first module (providing a module path is not needed because we don't have any dependencies with other modules)
javac -d mods/nl.craftsmen.burgers.restaurant src/nl.craftsmen.burgers.restaurant/nl/craftsmen/burgers/restaurant/api/Restaurant.java src/nl.craftsmen.burgers.restaurant/module-info.java

# create the service/burger consumer
javac --module-path mods -d mods/nl.craftsmen.burgers.consumer src/nl.craftsmen.burgers.consumer/nl/craftsmen/burgers/consumer/Main.java src/nl.craftsmen.burgers.consumer/module-info.java

#running the application
java --module-path mods -m nl.craftsmen.burgers.consumer/nl.craftsmen.burgers.consumer.Main

# create the macdonalds module
javac --module-path mods -d mods/nl.craftsmen.burgers.macdonalds src/nl.craftsmen.burgers.macdonalds/nl/craftsmen/burgers/macdonalds/internal/Kitchen.java src/nl.craftsmen.burgers.macdonalds/nl/craftsmen/burgers/macdonalds/MacDonalds.java src/nl.craftsmen.burgers.macdonalds/module-info.java

# create the BurgerKing module
javac --module-path mods -d mods/nl.craftsmen.burgers.burgerking src/nl.craftsmen.burgers.burgerking/nl/craftsmen/burgers/burgerking/internal/Kitchen.java src/nl.craftsmen.burgers.burgerking/nl/craftsmen/burgers/burgerking/BurgerKing.java src/nl.craftsmen.burgers.burgerking/module-info.java

#running the application
java --module-path mods -m nl.craftsmen.burgers.consumer/nl.craftsmen.burgers.consumer.Main