# create the macDonalds module
javac --module-path mods -d mods/nl.craftsmen.burgers.macdonalds src/nl.craftsmen.burgers.macdonalds/nl/craftsmen/burgers/macdonalds/internal/Kitchen.java src/nl.craftsmen.burgers.macdonalds/nl/craftsmen/burgers/macdonalds/MacDonalds.java src/nl.craftsmen.burgers.macdonalds/module-info.java

# create the consumer main module
javac --module-path mods -d mods/nl.craftsmen.burgers.consumer src/nl.craftsmen.burgers.consumer/nl/craftsmen/burgers/consumer/Main.java src/nl.craftsmen.burgers.consumer/module-info.java

# and run the consumer main module
java --module-path mods -m nl.craftsmen.burgers.consumer/nl.craftsmen.burgers.consumer.Main

result: the BigMac is served!!!

# create the naughty consumer module
# compilation will fail because the naugthycustomer's main.class directly accesses Kitchen.class from module nl.craftsmen.burgers.macdonalds which is not exported
javac --module-path mods -d mods/nl.craftsmen.burgers.naughtyconsumer src/nl.craftsmen.burgers.naughtyconsumer/nl/craftsmen/burgers/naughtyconsumer/Main.java src/nl.craftsmen.burgers.naughtyconsumer/module-info.java

#packaging as a module 
#(when running like this make sure you remove the module dir with content else you will get an exception that two versions of the module are on the module path)
jar --create --file=mods/nl.craftsmen.burgers.consumer.jar --main-class=nl.craftsmen.burgers.consumer.Main -C mods/nl.craftsmen.burgers.consumer .
jar --create --file=mods/nl.craftsmen.burgers.macdonalds.jar -C mods/nl.craftsmen.burgers.macdonalds .
# alternative for running the application after packaging with assigning a main class to the jmod jar
java --module-path mods -m nl.craftsmen.burgers.consumer

# old way of compiling code from within the source module directory 
javac -d ../../bin nl/craftsmen/burgers/macdonalds/internal/Kitchen.java nl/craftsmen/burgers/macdonalds/MacDonalds.java
javac -cp ../../bin -d ../../bin nl/craftsmen/burgers/consumer/Main.java

# running jdeps on a module
jdeps -s --module-path mods/ mods/nl.craftsmen.burgers.consumer.jar
nl.craftsmen.burgers.consumer -> java.base
nl.craftsmen.burgers.consumer -> nl.craftsmen.burgers.macdonalds
nl.craftsmen.burgers.consumer -> not found

#running Jlink for creating an minimal image (depends on os and location of your jdk9 installation)
jlink --module-path d:\Java\jdk9-ea-133\jmods;mods --add-modules nl.craftsmen.burgers.consumer --output image