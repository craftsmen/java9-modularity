package nl.craftsmen.burgers.macdonalds;

import nl.craftsmen.burgers.macdonalds.internal.Kitchen;

public class MacDonalds {
	private Kitchen kitchen = new Kitchen();
	
	public String serveBurger(){
		return kitchen.createBurger();
	}
}