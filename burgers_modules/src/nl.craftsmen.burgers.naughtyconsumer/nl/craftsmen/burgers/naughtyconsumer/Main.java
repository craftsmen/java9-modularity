package nl.craftsmen.burgers.naughtyconsumer;

import nl.craftsmen.burgers.macdonalds.internal.Kitchen;

public class Main {

	public static void main(String[] args) {
		Kitchen kitchen = new Kitchen();
		System.out.println("Naughty consumer sais: Macdonalds serverd me a " + kitchen.createBurger());
	}
}